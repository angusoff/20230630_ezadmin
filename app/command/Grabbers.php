<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log ;
use think\facade\Db ;

use app\model\collect\Umaevents;

/**
 * Class Grabbers
 * 1.建立新的connand 程式
 *  > php think make:command Grabbers
 *
 * 2.在/config/console.php添加指令定義
 *  'commands' => [
		'hello'     => 'app\command\Hello',
		'console'   => 'app\command\Console',
		'import'    => 'app\command\Import',
		'Grabbers'  => 'app\command\Grabbers', <<<<<<<<<
		],
 *
 * 3.執行(在config/console.php中 前綴詞[key]有分大小寫)
	/www/server/php/73/bin/php think Grabbers
	[msg]: app\command\Grabbers
 *
 * 4.php think Grabbers --help 檢視可用參數
 *
 * 5.執行功能
 *  > php think Grabbers --task=uma --site=umaevents
 * [msg]:app\command\download
		執行[] 檔案下載
		in func [download_plvr_land]
 *
 */
class Grabbers extends Command
{
	public $input ;
	public $output ;

    protected function configure()
    {
        // 指令配置
		// Option::VALUE_NONE 參數不帶值
		// Option::VALUE_REQUIRED 參數必須帶值
		// Option::VALUE_OPTIONAL 參數可選帶值
        $this->setName('grabbers')
		->addOption('task', null, Option::VALUE_REQUIRED, "任務名稱")
		->addOption('site', null, Option::VALUE_REQUIRED, "站點")
		->setDescription('采集器 the grabbers command');
    }

    protected function execute(Input $input, Output $output)
    {
		// 執行程式的開始時間 在console mode中印出來 YYYY-MM-DD HH:ii:ss
		$startTime = date('Y-m-d H:i:s') ;
		$msg = "[采集器] ".PHP_EOL ;
		$msg .= "開始時間: ".$startTime.PHP_EOL ;
		// 指令输出
		$output->writeln( $msg);
		// 寫入日誌log
		Log::channel('grabbers')->write( $msg, 'info') ;
		$this->input = $input ;
		$this->output = $output ;

		/*|--------------------------------------------------------------------
		|*| 取得外部參數
		|*| task : 任務名稱
		|*| site : 站點
		|*|--------------------------------------------------------------------*/
		// 檢查是否有參數	$input->hasOption( '參數名稱') ;
		// 取出參數值		$input->getOption( '參數名稱') ;
		$params = [] ;
		if ( $input->hasOption( 'task')) {
			$params['task'] = $input->getOption( 'task') ;
		}
		if ( $input->hasOption( 'site')) {
			$params['site'] = $input->getOption( 'site') ;
		}
		// $ins = [] ;
		// $ins['name'] = "angus" ;
		// Db::name('uma_girls')->insert( $ins) ;

		/*|--------------------------------------------------------------------
		|*| 執行任務
		|*|--------------------------------------------------------------------*/
		if ( !empty($params['task']) ) {
			$this->runMission( $params) ;
		} else {
			$msg = "[采集器] php think Grabbers site=umaevents ".PHP_EOL ;
			// 指令输出
			$output->writeln( $msg);
		}
	}


	// $msg = "[采集器] php think Grabbers site=umaevents ".PHP_EOL ;

	// // 指令输出
	// $output->writeln( $msg);

	// new Umaevents() ;
	/**
	 * [runMission 采集選擇器]
	 *
	 * @param  array $params
	 * @return void
	 */
	public function runMission( $params = []) {

		/*|--------------------------------------------------------------------
		|*| 任務選擇器 todo
		|*|--------------------------------------------------------------------*/
		if ( !empty($params['task']) && !empty($params['site'])) {
			$task = $params['task'] ;
			$site = ucfirst( $params['site']) ;
			$this->output->writeln( $site);
			$missObj = new Umaevents() ;
			$missObj->runMission() ;
		}
	}
} // end of class