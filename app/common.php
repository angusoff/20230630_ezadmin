<?php
// 应用公共文件

use app\common\service\AuthService;
use think\facade\Cache;
use think\facade\Env;


if (!function_exists('__url')) {

    /**
     * 构建URL地址
     * @param string $url
     * @param array $vars
     * @param bool $suffix
     * @param bool $domain
     * @return string
     */
    function __url(string $url = '', array $vars = [], $suffix = true, $domain = false)
    {
        return url($url, $vars, $suffix, $domain)->build();
    }
}

if (!function_exists('password')) {

    /**
     * 密码加密算法
     * @param $value 需要加密的值
     * @param $type  加密类型，默认为md5 （md5, hash）
     * @return mixed
     */
    function password($value)
    {
        $value = sha1('blog_') . md5($value) . md5('_encrypt') . sha1($value);
        return sha1($value);
    }

}

if (!function_exists('xdebug')) {

    /**
     * debug调试
     * @deprecated 不建议使用，建议直接使用框架自带的log组件
     * @param string|array $data 打印信息
     * @param string $type 类型
     * @param string $suffix 文件后缀名
     * @param bool $force
     * @param null $file
     */
    function xdebug($data, $type = 'xdebug', $suffix = null, $force = false, $file = null)
    {
        !is_dir(runtime_path() . 'xdebug/') && mkdir(runtime_path() . 'xdebug/');
        if (is_null($file)) {
            $file = is_null($suffix) ? runtime_path() . 'xdebug/' . date('Ymd') . '.txt' : runtime_path() . 'xdebug/' . date('Ymd') . "_{$suffix}" . '.txt';
        }
        file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] " . "========================= {$type} ===========================" . PHP_EOL, FILE_APPEND);
        // $str = (is_string($data) ? $data : (is_array($data) || is_object($data)) ? print_r($data, true) : var_export($data, true)) . PHP_EOL;
        $str = is_string( $data) ? $data : (( is_array( $data) || is_object( $data)) ? print_r( $data, true) : var_export( $data, true) . PHP_EOL) ;
        $force ? file_put_contents($file, $str) : file_put_contents($file, $str, FILE_APPEND);
    }
}

if (!function_exists('sysconfig')) {

    /**
     * 获取系统配置信息
     * @param $group
     * @param null $name
     * @return array|mixed
     */
    function sysconfig($group, $name = null)
    {
        $where = ['group' => $group];
        $value = empty($name) ? Cache::get("sysconfig_{$group}") : Cache::get("sysconfig_{$group}_{$name}");
        if (empty($value)) {
            if (!empty($name)) {
                $where['name'] = $name;
                $value = \app\admin\model\SystemConfig::where($where)->value('value');
                Cache::tag('sysconfig')->set("sysconfig_{$group}_{$name}", $value, 3600);
            } else {
                $value = \app\admin\model\SystemConfig::where($where)->column('value', 'name');
                Cache::tag('sysconfig')->set("sysconfig_{$group}", $value, 3600);
            }
        }
        return $value;
    }
}

if (!function_exists('array_format_key')) {

    /**
     * 二位数组重新组合数据
     * @param $array
     * @param $key
     * @return array
     */
    function array_format_key($array, $key)
    {
        $newArray = [];
        foreach ($array as $vo) {
            $newArray[$vo[$key]] = $vo;
        }
        return $newArray;
    }

}

if (!function_exists('auth')) {

    /**
     * auth权限验证
     * @param $node
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function auth($node = null)
    {
        $authService = new AuthService(session('admin.id'));
        $check = $authService->checkNode($node);
        return $check;
    }

}

/*|--------------------------------------------------------------------------
|*| 這裡是我自己加的共用文件
|*|--------------------------------------------------------------------------*/
function mac_arr2file($f,$arr='')
{
	if(is_array($arr)){
		$con = var_export($arr,true);
	} else{
		$con = $arr;
	}
	$con = "<?php\nreturn $con;";
	mac_write_file($f, $con);
}
function mac_write_file($f,$c='')
{
	$dir = dirname($f);
	if(!is_dir($dir)){
		mac_mkdirss($dir);
	}
	return @file_put_contents($f, $c);
}
function mac_mkdirss($path,$mode=0777)
{
	if (!is_dir(dirname($path))){
		mac_mkdirss(dirname($path));
	}
	if(!file_exists($path)){
		return mkdir($path,$mode);
	}
	return true;
}

if (!function_exists('getGUID')) {
	/**
	 * [getGUID description]
	 * @param   boolean    $mod [description]
	 * @return  [type]          [description]
	 * @Another Angus
	 * @date    2021-01-11
	 */
	function getGUID( $mod = true) {
		if (function_exists('com_create_guid')){
			return com_create_guid();
		} else {
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			if ( $mod) {
				$uuid = chr(123)// "{"
					.substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12)
					.chr(125);// "}"
			} else {
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12) ;
			}
			return $uuid;
		}
	}
}

if (!function_exists('sendLineNotify')) {
	function sendLineNotify ( $msgStr = "", $send = null) {
		$sendFlag = false ;
		// dumpa( [$token, $msgStr]) ;
		/**
		 * [$notifyToken Line notify 設定值]
		 * @var string
		 */
		if ( $send) {
			$sendFlag = true ;
		}

		if ( Env::get('line.supervision')) {
			$sendFlag = true ;
		}

		if ( $sendFlag) {
			$notifyToken = Env::get('line.token') ; // 主機狀態通知
			$notifyUrl   = 'https://notify-api.line.me/api/notify -H "Authorization: Bearer %s" -d "message=%s"' ;
			$notifyUrlStr = sprintf( $notifyUrl, $notifyToken, $msgStr) ;
			exec( "curl {$notifyUrlStr}") ;
		}
	}
}

// CurlPOST数据提交-----------------------------------------
function mac_curl_post($url,$data,$heads=array(),$cookie='')
{
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36');
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLINFO_CONTENT_LENGTH_UPLOAD,strlen($data));
	curl_setopt($ch, CURLOPT_HEADER,0);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);

	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	if(!empty($cookie)){
		curl_setopt($ch, CURLOPT_COOKIE, $cookie);
	}
	if(count($heads)>0){
		curl_setopt ($ch, CURLOPT_HTTPHEADER , $heads );
	}
	$response = @curl_exec($ch);
	if(curl_errno($ch)){//出错则显示错误信息
		//print curl_error($ch);
	}
	curl_close($ch); //关闭curl链接
	return $response;//显示返回信息
}
// CurlPOST数据提交-----------------------------------------
function mac_curl_get($url,$heads=array(),$cookie='')
{
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_HEADER,0);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT_MS, 120000); // 設定最長執行 900 毫秒
	if(!empty($cookie)){
		curl_setopt($ch, CURLOPT_COOKIE, $cookie);
	}
	if(count($heads)>0){
		curl_setopt ($ch, CURLOPT_HTTPHEADER , $heads );
	}
	$response = @curl_exec($ch);
	if(curl_errno($ch)){//出错则显示错误信息
		//print curl_error($ch);die;
		dump( curl_error($ch)) ;
	}
	curl_close($ch); //关闭curl链接
	return $response;//显示返回信息
}