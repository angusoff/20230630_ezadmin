<?php
/*|--------------------------------------------------------------------------
|*| model建立
|*| php think make:model common@Vcurl
|*| 就會在/app/common/model/建立Vcurl.php
|*|--------------------------------------------------------------------------*/

declare (strict_types = 1);

namespace app\common\model;

use think\Model;
use think\facade\Log ;

/**
 * @mixin \think\Model
 */
class Vcurl extends Model
{

    /*|--------------------------------------------------------------------------
    |*| 初始設定
    |*|--------------------------------------------------------------------------*/
    public $headers   = [] ;
    public $useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36';
    public $timeout   = 30 ;
    //
    public function __construct()
    {
        // 這裡印出我在這裡的文字在command中
        // echo "[Vcurl] __construct".PHP_EOL ;
    }

    /**
     * [saveToString 瀏覽網頁以文字方式回傳]
     * @param  string  $url       [網址]
     * @param  string  $cookie    [cookie]
     * @param  string  $post      [post]
     * @param  string  $referer   [引薦頁面 網址]
     * @param  string  $useragent [使用者代理]
     * @param  string  $saveFile  [儲存檔案]
     */
    public function saveToString( $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = '' ) {
        // $msg = "[Vcurl] saveToString: {$url}".PHP_EOL ;
        // echo $msg ;
        // Log::channel('grabbers')->write( $msg, 'info') ;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); // 0 = Don't give me the return header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 1 = return page
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_USERAGENT, isset($useragent) ? $useragent : $this->useragent);
        if($this->headers){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        }
        if ($cookie) {
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }
        if ($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        if ($referer) {
            curl_setopt($ch, CURLOPT_REFERER, $referer);
        }
        $content = curl_exec($ch);
        /*|--------------------------------------------------------------------------
        |*| 這裡是紀錄錯誤
        |*|--------------------------------------------------------------------------*/
        if (curl_errno($ch)) {
            Log::channel('grabbers')->write( curl_error($ch), 'error') ;
        }
        if ( curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 ) {
            Log::channel('grabbers')->write( curl_getinfo($ch), 'error') ;
        }
        curl_close($ch);

        if ($saveFile) {
            file_put_contents($saveFile, $content);
        }
        return $content;
    }


    public function saveToImage( $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = '') {
        // $msg = "[Vcurl] saveToImage: {$url}".PHP_EOL ;
        // echo $msg ;
        // Log::channel('grabbers')->write( $msg, 'info') ;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0); // 0 = Don't give me the return header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 1 = return page
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_USERAGENT, isset($useragent) ? $useragent : $this->useragent);
        if($this->headers){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        }
        if ($cookie) {
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }
        if ($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        if ($referer) {
            curl_setopt($ch, CURLOPT_REFERER, $referer);
        }
        $content = curl_exec($ch);
        /*|--------------------------------------------------------------------------
        |*| 這裡是紀錄錯誤
        |*|--------------------------------------------------------------------------*/
        if (curl_errno($ch)) {
            Log::channel('grabbers')->write( curl_error($ch), 'error') ;
        }
        if ( curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200 ) {
            Log::channel('grabbers')->write( curl_getinfo($ch), 'error') ;
        }
        curl_close($ch);

        if ($saveFile) {
            $directory = dirname($saveFile) ;
            if (!is_dir($directory)) {
                mkdir($directory, 0777, true);
            }
            file_put_contents($saveFile, $content);
        }
        return $content;

    }
}
