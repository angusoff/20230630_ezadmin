<?php
/*|--------------------------------------------------------------------------
|*| model建立
|*| php think make:model collect/Caiji
|*| 就會在/app/model/collect/建立Caiji.php
|*|--------------------------------------------------------------------------*/
declare (strict_types = 1);

namespace app\model\collect;

use think\Model;
use think\facade\Env;
use think\facade\Log ;
use app\common\model\Vcurl ;
use app\common\model\phpQuery ;


/**
 * @mixin \think\Model
 */
class Caiji extends Model
{
    // 跳版網址
    public $relaySite   = "http://114.34.22.240:7000/getPage.php?url=%s" ;
    public $relayImage  = "http://114.34.22.240:7000/getPage.php?mod=img&url=%s" ;
    public $curl = null;
    public $qpDocument = null;


    //
    public function __construct()
    {
        // 這裡印出我在這裡的文字在command中
        // $msg = "[Caiji] __construct".PHP_EOL ;
        // echo $msg ;
        // Log::channel('grabbers')->write( $msg, 'info') ;
        $this->curl = new Vcurl() ;
    }

    /**
     * [getPage 瀏覽網頁以文字方式回傳]
     * @param  string  $url       [網址]
     * @param  string  $cookie    [cookie]
     * @param  string  $post      [post]
     * @param  string  $referer   [引薦頁面 網址]
     * @param  string  $useragent [使用者代理]
     * @param  string  $saveFile  [儲存檔案]
     */
    public function getPage( $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = '') {
        $msg = "[Caiji] getPage: {$url}".PHP_EOL ;
        echo $msg ;
        Log::channel('grabbers')->write( $msg, 'info') ;
        return $this->curl->saveToString( $url, $cookie, $post, $referer, $useragent, $saveFile) ;
    }

    public function getImgage( $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = '') {
        // $msg = "[Caiji] getImgage: {$url}".PHP_EOL ;
        // echo $msg ;
        // Log::channel('grabbers')->write( $msg, 'info') ;
        return $this->curl->saveToImage( $url, $cookie, $post, $referer, $useragent, $saveFile) ;
    }

    /**
     * [getNodeElement 取得節點元素] 這裡可以純化要取得的節點元素 就不會整個網頁查找
     *
     * @param string $selectorStr [網頁元素]
     * @param string $nodeindex [節點座標]
     * @return void
     */
    public function getNodeElement($selectorStr = "", $mainNode = null, $nodeindex = '') {
        // 如果有主節點 就從主節點開始找
        if ( $mainNode) {
            $qpObj = $mainNode ;
        } else { // 沒有就從網頁開始找
            $qpObj = $this->qpDocument ;
        }

        if ( !empty($nodeindex)) {
            $name_obj = pq($selectorStr, $qpObj)->eq($nodeindex);
        } else {
            $name_obj = pq($selectorStr, $qpObj);
        }

        return $name_obj;
    }

    /**
     * [getPageElement 取得網頁元素]
     *
     * @param string $selectorStr [網頁元素]
     * @param [type] $nodeArea [節點元素]
     * @param boolean $getfirst [是否取得第一個]
     * @return void
     */
    public function getPageElement($selectorStr = "", $nodeArea = null, $getfirst = false) {
        if ( $nodeArea) { // 如果有主節點 就從主節點開始找
            $qpObj = $nodeArea ;
        } else { // 沒有就從網頁開始找
            $qpObj = $this->qpDocument ;
        }

        if ( $getfirst) {
            $name_obj = pq($selectorStr, $qpObj)->eq(0);
        } else {
            $name_obj = pq($selectorStr, $qpObj);
        }

        return !empty( trim($name_obj->text())) ? $name_obj->text() : $name_obj;
    }


}
