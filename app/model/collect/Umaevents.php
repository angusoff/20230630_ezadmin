<?php
/*|--------------------------------------------------------------------------
|*| 賽馬娘釆集器 參考網址
|*|--------------------------------------------------------------------------
|*| https://umaevents.io
|*| https://forum.gamer.com.tw/A.php?bsn=34421
|*| https://home.gamer.com.tw/artwork.php?sn=5735217
|*| https://wiki.biligame.com/umamusume/%E9%A6%96%E9%A1%B5
|*|
|*|
|*|
|*| 執行方式 php think Grabbers --task=mua --site=umaevents
|*|--------------------------------------------------------------------------*/
declare (strict_types = 1);

namespace app\model\collect;

use think\Model;
use think\facade\Db ;
use think\facade\Log ;
use think\console\Output ;
use app\common\model\Vcurl ;
use app\common\model\phpQuery ;

/**
 * @mixin \think\Model
 */
class Umaevents extends Caiji
{
	private $roles ;			// 角色列表
    private $dataPath ;			// 資料儲存路徑 /runtime/grabbers/umaevents
	private $dataPathDocs ;
	private $dataPathAvatars ; 	// 角色頭像儲存路徑


	private $siteUrl				= "https://umaevents.io" ;
	private $pageGirlsIndexUrl		= "https://umaevents.io/zh-TW/uma" ; 		// 角色列表頁面 Role ： 角色
	private $pageSkillIndexUrl		= "https://umaevents.io/zh-TW/skill" ;		// 技能列表頁面
	private $pageSupportIndexUrl	= "https://umaevents.io/zh-TW/support" ;	// 支援列表頁面
    //
    public function __construct()
    {
        parent::__construct();
        // 這裡印出我在這裡的文字在command中
        // $msg = "[Umaevents] __construct".PHP_EOL ;
        // echo $msg ;
        // Log::channel('grabbers')->write( $msg, 'info') ;
        $this->dataPath 		= runtime_path('grabbers/umaevents') ;					// 專案資料儲存路徑
		$this->dataPathDocs		= runtime_path('grabbers/umaevents/docs') ;				// 專案網頁儲存路徑
		$this->dataPathAvatars	= runtime_path('grabbers/umaevents/images/Avatar') ;	// 角色頭像儲存路徑
		$this->dataPathSkills	= runtime_path('grabbers/umaevents/images/skills') ;	// 技能儲存路徑
		$this->dataPathSupport	= runtime_path('grabbers/umaevents/images/support') ;	// 支援儲存路徑

		// $msg = PHP_EOL."[采集器 - Umaevents] ".PHP_EOL ;
		// $msg .= "開始時間: ".date("Y-m-d H:i:s").PHP_EOL ;
		// sendLineNotify( $msg) ;
	}

    /**
     * [runMission 執行釆集任務]
     *
     * @return void
     */
    public function runMission()
    {
        // 時間上限的部份單位是秒，如果要設為無上限，可以設定0
        set_time_limit(0);

		$runStart = date('Y-m-d H:i:s') ;
//		$this->getGirlsList() ;		// 取得角色列表
		$this->getGirlsInfo() ;		// 取得角色資訊
//		$this->getGirlsSkill() ;	// 取得角色技能
//		$this->getSupport() ;	    // 取得角色支援
        // $this->mappingInfoSkillSupport() ;    // 將角色資訊、技能、支援做關聯

		$msg = PHP_EOL."[采集器 - Umaevents] ".PHP_EOL ;
		$msg .= "結束時間: ".date('Y-m-d H:i:s').PHP_EOL ;
		echo $msg ;
		Log::channel('grabbers')->write( $msg, 'info') ;
    }

    /**
     * [getGirlsList 取得角色列表]
     *
     * @return void
     */
    public function getGirlsList()
    {
        $msg = "[Umaevents] getGirlsList".PHP_EOL ;
        echo $msg ;
        Log::channel('grabbers')->write( $msg, 'info') ;
        $girlsListPage = $this->dataPathDocs."rolesList_jp.html" ;
        $girlsListPageTW = $this->dataPathDocs."rolesList_tw.html" ;
        $girlsListPageCN = $this->dataPathDocs."rolesList_cn.html" ;
        $girlsListPageKR = $this->dataPathDocs."rolesList_kr.html" ;
		// var_dump( $girlsListPage) ; exit() ;
		$girlsListurl = $this->pageGirlsIndexUrl.urlencode("?server=jp") ; // 釆集角色列表頁面

        /*|--------------------------------------------------------------------------
        |*| 如果沒有角色列表頁面，就去抓取
        |*|--------------------------------------------------------------------------*/
        if ( !file_exists( $girlsListPage) || filesize( $girlsListPage) == 0) {
            $readUrl = sprintf( $this->relaySite, $girlsListurl) ; // 經過relaySite的網址
			/*|--------------------------------------------------------------------------
			|*| 這裡的 $readUrl 是經過relaySite的網址
			|*| https://umaevents.io/zh-TW/uma
			|*| 變成
			|*| http://114.34.22.240:7000/getPage.php?url=https://umaevents.io/zh-TW/uma
			|*| $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = ''
			|*|--------------------------------------------------------------------------*/
            $content = $this->getPage( $readUrl) ;
            $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $girlsListPage, $content) ;	// 寫入檔案
			// 繁中
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageGirlsIndexUrl.urlencode("?server=tw"))) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $girlsListPageTW, $content) ;	// 寫入檔案
            // 簡中
            $content = $this->getPage( sprintf( $this->relaySite, $this->pageGirlsIndexUrl.urlencode("?server=cn"))) ;
            $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $girlsListPageCN, $content) ;	// 寫入檔案
			// 韓文
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageGirlsIndexUrl.urlencode("?server=ko"))) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $girlsListPageKR, $content) ;	// 寫入檔案

            Log::channel('grabbers')->write( "girlsList.html 下載完成", 'info') ;
			$dbGirls = [] ;
        } else {
            $msg = "[Umaevents] getGirlsList: girlsListPage is exists".PHP_EOL ;
			/*|--------------------------------------------------------------------------
			|*| 如果有角色列表頁面，就去讀取
			|*|--------------------------------------------------------------------------*/
			$dbGirls = Db::name('uma_girls')->select()->toArray();
        }

        // 繁中
        $content = file_get_contents( $girlsListPageTW) ;
        $pageObj = \phpQuery::newDocumentHTML( $content) ;
        $girlsListTW = $pageObj->find("div.grid a") ;
        // 簡中
        $content = file_get_contents( $girlsListPageCN) ;
        $pageObj = \phpQuery::newDocumentHTML( $content) ;
        $girlsListCN = $pageObj->find("div.grid a") ;
        // 韓文
        $content = file_get_contents( $girlsListPageKR) ;
        $pageObj = \phpQuery::newDocumentHTML( $content) ;
        $girlsListKR = $pageObj->find("div.grid a") ;
        // 日文
        $girls = [] ;
        $content = file_get_contents( $girlsListPage) ;
        $pageObj = \phpQuery::newDocumentHTML( $content) ;
        $girlsList = $pageObj->find("div.grid a") ;

		// var_dump( count( $girlsList)) ; exit() ;

		$saveImagePath = $this->dataPathAvatars ;
        foreach ($girlsList as $iCnt => $node) {
            $girlNode = [] ;
            $girlNode['grab_url']		= $this->siteUrl.pq($node)->attr('href') ;
			$girlNode['grab_id']		= basename( pq($node)->attr('href')) ;
            $girlNode['name']			= pq($node)->find('div.text-2xl')->text() ;
            $girlNode['imgAvatarPath']	= pq($node)->find('imgscript img')->attr('src') ;

			/*|--------------------------------------------------------------------------
			|*| 檢查馬娘是否已經存在, 如果存在就檢查頭像是否存在, 如果頭像不存在就去抓取
			|*|--------------------------------------------------------------------------*/
			$isAvatarExists = false ;
			$isRolesExists = false ;
			foreach ( $dbGirls as $gIdx => $tmpGInfo) {
                if ( $tmpGInfo['name'] == $girlNode['name']) {
					$isRolesExists = true ;
					$girlNode['dbAvatar'] = $tmpGInfo['avatar'] ;
					if ( file_exists( $saveImagePath.$tmpGInfo['avatar'])) {
						$isAvatarExists = true ;
						$msg = "[Umaevents] getGirlsList: {$girlNode['name']} 頭像已存在".PHP_EOL ;
						// echo $msg ;
						// Log::channel('grabbers')->write( $girlNode['name'] . " 頭像已存在", 'info') ;
					}
					break ;

				}
			}

            // 檢查繁中清單裡是否有相同名稱的馬娘
            $ins_tw = 0;
            foreach ($girlsListTW as $nodeTW) {
                if (pq($nodeTW)->find('div.text-2xl')->text() == $girlNode['name']) {
                    $ins_tw = 1;
                    break;
                }
            }
            // 檢查簡中清單裡是否有相同名稱的馬娘
            $ins_cn = 0;
            foreach ($girlsListCN as $nodeCN) {
                if (pq($nodeCN)->find('div.text-2xl')->text() == $girlNode['name']) {
                    $ins_cn = 1;
                    break;
                }
            }
            // 檢查韓文清單裡是否有相同名稱的馬娘
            $ins_kr = 0;
            foreach ($girlsListKR as $nodeKR) {
                if (pq($nodeKR)->find('div.text-2xl')->text() == $girlNode['name']) {
                    $ins_kr = 1;
                    break;
                }
            }

            /* 新角色
            array(3) {
                'grab_url' => "/zh-TW/owner/agajpMZ1"
                'name' => "帝王光輝"
                'imgAvatarPath' => "/_image/owner/0dcb0073c48075133546056409c4c37a37b1cd9d.png?w=3840&q=80"
            }
            */
			if ( !$isRolesExists) {
				$msg = "[Umaevents] getGirlsList: {$girlNode['name']} 新增角色".PHP_EOL ;
				echo $msg ;
				Log::channel('grabbers')->write( $girlNode['name'] . " 新增角色", 'info') ;

				$queryStr = parse_url( $girlNode['imgAvatarPath'], PHP_URL_QUERY) ; // 取得query string
				$avatarName = basename( str_replace( '?'.$queryStr, '' , $girlNode['imgAvatarPath'])) ; // 取得檔名
				$extension = pathinfo($avatarName, PATHINFO_EXTENSION) ; // 取得副檔名
				$newAvatarName = getGUID( false).".".$extension ; // 產生新檔名
				$saveFile = $saveImagePath.$newAvatarName ; // 完整路徑

				$imgUrl = $this->siteUrl.$girlNode['imgAvatarPath'] ; // 釆集圖片的網址
				$grabUrl = sprintf( $this->relayImage, $imgUrl) ; // 使用agent釆集圖片的網址

				/*|--------------------------------------------------------------------------
				|*| $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = ''
				|*|--------------------------------------------------------------------------*/
				$referer = $girlsListurl ;
				$this->getImgage( $grabUrl, '', '', $referer, '', $saveFile) ;
				$girlNode['imgAvatar'] = $newAvatarName ;

				$ins = [] ;
				$ins['name'] = $girlNode['name'] ;
				$ins['avatar'] = $girlNode['imgAvatar'] ;
				$ins['grab_id'] = $girlNode['grab_id'] ;
				$ins['grab_url'] = $girlNode['grab_url'] ;
				$ins['jp'] = 1 ;
                $ins['tw'] = $ins_tw;
                $ins['cn'] = $ins_cn;
                $ins['kr'] = $ins_kr;

				try {
					Db::name('uma_girls')->insert($ins) ;
					$isAvatarExists = true ;
				} catch ( \Throwable $th) {
					$SQLCmd = Db::getLastSql();
					Log::channel('grabbers')->write( $SQLCmd, 'mysql') ;
					exit() ;
				}
			}

			/*|--------------------------------------------------------------------------
			|*| 如果角色存在, 但頭像不存在, 就去抓取頭像
			|*|--------------------------------------------------------------------------*/
			if ( !$isAvatarExists) {
				$queryStr = parse_url( $girlNode['imgAvatarPath'], PHP_URL_QUERY) ; // 取得query string
				$avatarName = basename( str_replace( '?'.$queryStr, '' , $girlNode['imgAvatarPath'])) ; // 取得檔名
				$saveFile = $saveImagePath.$girlNode['dbAvatar'] ; // 完整路徑

				$imgUrl = $this->siteUrl.$girlNode['imgAvatarPath'] ; // 釆集圖片的網址
				$grabUrl = sprintf( $this->relayImage, $imgUrl) ; // 使用agent釆集圖片的網址
				$referer = $girlsListurl ;
				$this->getImgage( $grabUrl, '', '', $referer, '', $saveFile) ;
			}
        }
    }

	/**
	 * [getGirlsInfo 取得個別角色資訊]
	 *
	 * @return void
	 */
	public function getGirlsInfo()
	{
		$msg = "[Umaevents] getGirlsInfo".PHP_EOL ;
		echo $msg ;
		Log::channel('grabbers')->write( $msg, 'info') ;
		$this->roles = Db::name('uma_girls')->select()->toArray();

		// $this->buildGirlInfo( $this->roles[8]) ;
        $newGirlCnt = 0 ;
        $totalGirlCnt = count($this->roles) ;
        echo "馬娘共有： ".$totalGirlCnt." 筆".PHP_EOL ;
        foreach( $this->roles as $rCnt => $girlNode) {
//if( $newGirlCnt == 45)
			$this->buildGirlInfo( $girlNode) ;
            $msg = "[Umaevents] uma_girls TW: new girl [".($newGirlCnt+1)."/".$totalGirlCnt."]".PHP_EOL ;
            $newGirlCnt++ ;
            echo $msg ;
//if( $newGirlCnt == 146) exit() ;
		}
	}

	/**
	 * [getGirlsSkill 取得所有角色技能]
	 *
	 * @return void
	 */
	public function getGirlsSkill()
	{
		$msg = "[Umaevents] getGirlsSkill".PHP_EOL ;
		echo $msg ;
		Log::channel('grabbers')->write( $msg, 'info') ;

		/*|--------------------------------------------------------------------------
		|*| 取回列表頁面
		|*|--------------------------------------------------------------------------*/
		$listPage = $this->dataPathDocs."skill_list_jp.html" ;
		if ( !file_exists( $listPage) || filesize( $listPage) == 0) {
			$grabUrl = sprintf( $this->relaySite, $this->pageSkillIndexUrl.urlencode("?server=jp")) ;
			$referer = $this->pageGirlsIndexUrl ;

            // 日文
			$content = $this->getPage( $grabUrl, '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $listPage, $content) ;	// 寫入檔案
			$referer = $this->pageSkillIndexUrl.urlencode("?server=jp") ;
			// 繁中
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageSkillIndexUrl.urlencode("?server=tw"))	, '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $this->dataPathDocs."skill_list_tw.html", $content) ;	// 寫入檔案
            // 簡中
            $content = $this->getPage( sprintf( $this->relaySite, $this->pageSkillIndexUrl.urlencode("?server=cn"))	, '', '', $referer) ;
            $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $this->dataPathDocs."skill_list_cn.html", $content) ;	// 寫入檔案
			// 韓文
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageSkillIndexUrl.urlencode("?server=ko"))	, '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $this->dataPathDocs."skill_list_kr.html", $content) ;	// 寫入檔案

		} else {
			// TODO
		}
		// 取得資料庫中已有的icon 透過array_column()轉換成一維陣列
		// 可以這麼寫 $iconMap = Db::table('ea_uma_skills')->distinct(true)->field('icon, s_icon')->select()->toArray();
		$iconMap = Db::name('uma_skills')->distinct(true)->field('icon, s_icon')->select()->toArray();
		$iconMap = array_column( $iconMap, 'icon', 's_icon') ;
		// var_dump($iconMap) ; exit() ;
		$skillList = Db::name('uma_skills')->field('id, grab_id')->select()->toArray();
		$skillList = array_column( $skillList, 'id', 'grab_id') ;
		// var_dump( $skillList) ; exit() ;

		// https://umaevents.io/zh-TW/skill/jNGVN5bH
		$content = file_get_contents( $this->dataPathDocs."skill_list_tw.html") ;
		$pageObj = \phpQuery::newDocumentHTML( $content) ;
		$objSkillList = $pageObj->find('.min-h-screen a.sr-only') ;

		var_dump( count($objSkillList)) ;
		$newSkillCnt = 0 ;
		foreach( $objSkillList as $sCnt => $skillNode) {
			$ins = [] ;
			$ins['s_name'] = pq('div div.h-full .font-bold', $skillNode)->text() ;
			$ins['s_summary'] = pq('div div.h-full .text-gray-400', $skillNode)->text() ;
			$ins['grab_url'] = $this->siteUrl.pq($skillNode)->attr('href') ;
			$ins['grab_id'] = basename( pq($skillNode)->attr('href')) ;
			// "/_image/skills/5b1db763347777094769e6972dccaf531249af41.png?w=3840&q=80"
			$sourceIconUrl = pq($skillNode)->find('imgscript img')->attr('src') ;

			$queryStr = parse_url( $sourceIconUrl, PHP_URL_QUERY) ; // 取得query string
			$sIconName = basename( str_replace( '?'.$queryStr, '' , $sourceIconUrl)) ; // 取得檔名
			$extension = pathinfo($sIconName, PATHINFO_EXTENSION) ; // 取得副檔名
			$newIconName = getGUID( false).".".$extension ; // 產生新檔名
			$saveFile = $this->dataPathSkills.$newIconName ; // 完整路徑

			$imgUrl = $this->siteUrl.$sourceIconUrl ; // 釆集圖片的網址
			$grabUrl = sprintf( $this->relayImage, $imgUrl) ; // 使用agent釆集圖片的網址

			if ( array_key_exists( $sIconName, $iconMap)) {
				// 如果已經抓過了，就不再抓了
				$ins['icon'] = $iconMap[$sIconName] ;
				$ins['s_icon'] = $sIconName ;
			} else {
				/*|--------------------------------------------------------------------------
				|*| $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = ''
				|*|--------------------------------------------------------------------------*/
				$referer = $grabUrl ;
				$this->getImgage( $grabUrl, '', '', $referer, '', $saveFile) ;
				$ins['icon'] = $newIconName ;
				$ins['s_icon'] = $sIconName ;
				$iconMap[$sIconName] = $newIconName ;
			}

            /*|--------------------------------------------------------------------------
			|*| 檢查技能檔是否已經存在
			|*|--------------------------------------------------------------------------*/
			$skillFile = $this->dataPathDocs."skills/".$ins['grab_id']."_tw.html" ;
			if ( !file_exists( $skillFile)) {
				// echo $ins['grab_id'].".html".PHP_EOL ;
				// 繁中
				$readUrl = sprintf( $this->relaySite, $ins['grab_url']) ; // 經過relaySite的網址
				$content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
				$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
				mac_write_file( $skillFile, $content) ;	// 寫入檔案

                // 簡中
                $skillFile = $this->dataPathDocs."skills/".$ins['grab_id']."_cn.html" ;
                $readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=cn")) ; // 經過relaySite的網址
                $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
                mac_write_file( $skillFile, $content) ;	// 寫入檔案
                // 取得簡中的技能 name & summary
                $content_cn = file_get_contents( $this->dataPathDocs."skills/".$ins['grab_id']."_cn.html") ;	// 讀取檔案
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_cn) ;
                $skillNode_cn = $this->qpDocument->find('.min-h-screen') ;
                $ins['s_name_cn'] = pq('div div.h-full .font-bold', $skillNode_cn)->text() ;
                $ins['s_summary_cn'] = pq('div div.h-full .text-gray-400', $skillNode_cn)->text() ;

				// 日文
				$skillFile = $this->dataPathDocs."skills/".$ins['grab_id']."_jp.html" ;
				$readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=jp")) ; // 經過relaySite的網址
				$content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
				$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
				mac_write_file( $skillFile, $content) ;	// 寫入檔案
                // 取得日文的技能 name & summary
                $content_jp = file_get_contents( $this->dataPathDocs."skills/".$ins['grab_id']."_jp.html") ;	// 讀取檔案
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_jp) ;
                $skillNode_jp = $this->qpDocument->find('.min-h-screen') ;
                $ins['s_name_jp'] = pq('div div.h-full .font-bold', $skillNode_jp)->text() ;
                $ins['s_summary_jp'] = pq('div div.h-full .text-gray-400', $skillNode_jp)->text() ;

				// 韓文
				$skillFile = $this->dataPathDocs."skills/".$ins['grab_id']."_kr.html" ;
				$readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=ko")) ; // 經過relaySite的網址
				$content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
				$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
				mac_write_file( $skillFile, $content) ;	// 寫入檔案
                // 取得韓文的技能 name & summary
                $content_kr = file_get_contents( $this->dataPathDocs."skills/".$ins['grab_id']."_kr.html") ;	// 讀取檔案
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_kr) ;
                $skillNode_kr = $this->qpDocument->find('.min-h-screen') ;
                $ins['s_name_kr'] = pq('div div.h-full .font-bold', $skillNode_kr)->text() ;
                $ins['s_summary_kr'] = pq('div div.h-full .text-gray-400', $skillNode_kr)->text() ;

				$msg = "{$ins['grab_id']}.html 下載完成" ;
				echo $msg.PHP_EOL ;
				Log::channel('grabbers')->write( "{$ins['grab_id']}.html 下載完成", 'info') ;
			}

            /*|--------------------------------------------------------------------------
            |*| 檢查是否已經存在
            |*|--------------------------------------------------------------------------*/
            if ( !array_key_exists( $ins['grab_id'], $skillList)) {
                try {
                    Db::name('uma_skills')->insert($ins) ;
                    $msg = "[Umaevents] getSkills TW: {$ins['s_name']} {$ins['grab_id']} new skill".PHP_EOL ;
                    echo $msg ;
                    Log::channel('grabbers')->write( $msg, 'info') ;
                    $newSkillCnt++ ;
                } catch ( \Throwable $th) {
                    $SQLCmd = Db::getLastSql();
                    Log::channel('grabbers')->write( $SQLCmd, 'mysql') ;
                    exit() ;
                }
            }

			// var_dump( $ins) ; exit() ;
			// if ( $sCnt > 10 ) exit() ;
		}

		/*|--------------------------------------------------------------------------
		|*| 結算筆數
		|*|--------------------------------------------------------------------------*/
		if ( $newSkillCnt > 0) {
			$msg = "[Umaevents] getSkills TW: ".$newSkillCnt." new skills".PHP_EOL ;
			echo $msg ;
			Log::channel('grabbers')->write( $msg, 'info') ;
		} else {
			$msg = "[Umaevents] getSkills TW: no new skills".PHP_EOL ;
			echo $msg ;
			Log::channel('grabbers')->write( $msg, 'info') ;
		}
	}

	public function getSupport()
	{
		$msg = "[Umaevents] getSupport".PHP_EOL ;
		echo $msg ;
		Log::channel('grabbers')->write( $msg, 'info') ;

		/*|--------------------------------------------------------------------------
		|*| 取回列表頁面
		|*|--------------------------------------------------------------------------*/
		$listPage = $this->dataPathDocs."support_list_jp.html" ;
		if ( !file_exists( $listPage) || filesize( $listPage) == 0) {
			$grabUrl = sprintf( $this->relaySite, $this->pageSupportIndexUrl.urlencode("?server=jp")) ;
			$referer = $this->pageGirlsIndexUrl ;

            // 日文
			$content = $this->getPage( $grabUrl, '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $listPage, $content) ;	// 寫入檔案
			$referer = $this->pageSupportIndexUrl.urlencode("?server=jp") ;
			// 繁中
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageSupportIndexUrl.urlencode("?server=tw")) , '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $this->dataPathDocs."support_list_tw.html", $content) ;	// 寫入檔案
            // 簡中
            $content = $this->getPage( sprintf( $this->relaySite, $this->pageSupportIndexUrl.urlencode("?server=cn")) , '', '', $referer) ;
            $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $this->dataPathDocs."support_list_cn.html", $content) ;	// 寫入檔案
			// 韓文
			$content = $this->getPage( sprintf( $this->relaySite, $this->pageSupportIndexUrl.urlencode("?server=ko")) , '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $this->dataPathDocs."support_list_kr.html", $content) ;	// 寫入檔案

			$msg = "support_list.html 下載完成" ;
			echo $msg.PHP_EOL ;
			Log::channel('grabbers')->write( "support_list.html 下載完成", 'info') ;

		} else {
			// TODO

        }

        // 取得資料庫中已有的avatar 透過array_column()轉換成一維陣列
        // 可以這麼寫 $iconMap = Db::table('ea_uma_support')->distinct(true)->field('icon, s_icon')->select()->toArray();
        $iconMap = Db::name('uma_support')->distinct(true)->field('avatar, s_avatar')->select()->toArray();
        $iconMap = array_column( $iconMap, 'avatar', 's_avatar') ;
        // var_dump($iconMap) ; exit() ;
        $supportList = Db::name('uma_support')->field('id, grab_id')->select()->toArray();
        $supportList = array_column( $supportList, 'id', 'grab_id') ;

		$content = file_get_contents( $this->dataPathDocs."support_list_jp.html") ;
		$pageObj = \phpQuery::newDocumentHTML( $content) ;
		$objSupList = $pageObj->find('.min-h-screen div div.grid a') ;

		$newSupCnt = 0 ;
        $totalSupCnt = count($objSupList) ;
        echo "支援卡共有： ".$totalSupCnt." 筆".PHP_EOL ;
		foreach( $objSupList as $sCnt => $tmpNode) {
			$ins = [] ;
            $ins_evt = [] ;

            $ins['grab_url'] = $this->siteUrl.pq($tmpNode)->attr('href') ;
			$ins['grab_id'] = basename( pq($tmpNode)->attr('href')) ;
			$sourceIconUrl = pq($tmpNode)->find('imgscript img')->attr('src') ;

			$queryStr = parse_url( $sourceIconUrl, PHP_URL_QUERY) ; // 取得query string
			$sIconName = basename( str_replace( '?'.$queryStr, '' , $sourceIconUrl)) ; // 取得檔名
			$extension = pathinfo($sIconName, PATHINFO_EXTENSION) ; // 取得副檔名
			$newIconName = getGUID( false).".".$extension ; // 產生新檔名
			$saveFile = $this->dataPathSupport.$newIconName ; // 完整路徑
			$imgUrl = $this->siteUrl.$sourceIconUrl ; // 釆集圖片的網址
			$grabUrl = sprintf( $this->relayImage, $imgUrl) ; // 使用agent釆集圖片的網址

            if ( array_key_exists( $sIconName, $iconMap)) {
                // 如果已經抓過了，就不再抓了
                echo "      已經抓過了，就不再抓了".PHP_EOL ;
                $ins['avatar'] = $iconMap[$sIconName] ;
                $ins['s_avatar'] = $sIconName ;
            } else {
                /*|--------------------------------------------------------------------------
                |*| $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = ''
                |*|--------------------------------------------------------------------------*/
                $referer = $grabUrl ;
                $this->getImgage( $grabUrl, '', '', $referer, '', $saveFile) ;
                $ins['avatar'] = $newIconName ;
                $ins['s_avatar'] = $sIconName ;
                $iconMap[$sIconName] = $newIconName ;
            }

            /*|--------------------------------------------------------------------------
			|*| 檢查支援檔是否已經存在
			|*|--------------------------------------------------------------------------*/
            $supportFile = $this->dataPathDocs."supports/".$ins['grab_id']."_jp.html" ;
            if ( !file_exists( $supportFile)) {
//            if (true) {
                // echo $ins['grab_id'].".html".PHP_EOL ;

                //
                // 日文 - 每一張支援卡
                //
                $supportFile = $this->dataPathDocs."supports/".$ins['grab_id']."_jp.html" ;
                $readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=jp")) ; // 經過relaySite的網址
                $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                if (empty($content)) {
                    for ($retry = 0; $retry <= 2; $retry++) {
                        echo "      重試中......\n";
                        $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                        if (!empty($content)) {
                            break;
                        }
                    }
                }
                $content = str_replace( "noscript", "imgscript", $content) ;	        // 把noscript改成imgscript
                mac_write_file( $supportFile, $content) ;	        // 寫入檔案
                // 取得日文的每一張支援卡的所有事件
                $content_jp = file_get_contents( $supportFile) ;	// 讀取檔案
                $content_jp = str_replace( "md:sr-only", "umaname", $content_jp) ;	    // 把md:sr-only改成umaname
                $content_jp = str_replace( "md:w-5/12", "supp_detail", $content_jp) ;	// 把md:w-5/12改成supp_detail
                $content_jp = str_replace( "md:w-4/12", "allevent", $content_jp) ;	    // 把md:w-4/12改成allevent
                $content_jp = str_replace( "md:w-8/12 ", "allevent2", $content_jp) ;	    // 把md:w-8/12改成all-event2
                $content_jp = str_replace( "md:w-9/12", "skill_effect", $content_jp) ;	// 把md:w-9/12改成skill_effect
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_jp) ;
                $supportNode_jp = $this->qpDocument->find('div .umaname .text-4xl');
                $ins['s_user_tw'] = pq($supportNode_jp)->text() ;
                $supportDetail_jp = $this->qpDocument->find('div.supp_detail');
//                echo "（（（（（（（（（（（（（（（（（（（（（\n";
//                dump(pq($supportDetail_jp)->html());
//                echo "）））））））））））））））））））））\n";
                // 支援卡明細
                foreach (pq('div.flex', $supportDetail_jp) as $item) {
                    $label = pq("div.mr-2", $item)->text() ;
                    $label = str_replace( "\n", "", $label) ;
                    $label2 = pq("div.px-1", $item)->text() ;
                    // 稀有度
                    if (strcmp($label, "稀有度") == 0) {
                        $ins['rarity'] = pq("div.text-lg", $item)->text() ;
                    }
                    // 類型
                    if (strcmp($label, "類型") == 0) {
                        $ins['genre'] = pq("div.text-lg", $item)->text() ;
                        $ins['genre_icon'] = pq($item)->find('imgscript img')->attr('src') ;
                        $ins['genre_icon'] = explode('?', basename($ins['genre_icon']))[0];
                    }
                    // 無凸評價
                    if (strcmp($label, "無凸評價") == 0) {
                        $ins['evaluation_no'] = pq("div.text-lg", $item)->text() ;
                    }
                    // 滿凸評價
                    if (strcmp($label, "滿凸評價") == 0) {
                        $ins['evaluation_full'] = pq("div.text-lg", $item)->text() ;
                    }
                    // 入手方式
                    if (strcmp($label2, "入手方式") == 0) {
                        $ins['obtain'] = pq("div.text-lg", $item)->text() ;
                    }
                }


                // 取得馬娘ID
                $dbGirls = Db::name('uma_girls')->field("id")->where("name", $ins['s_user_tw'])->select();
                if(count($dbGirls) > 0) {
                    $ins['girl_id'] = $dbGirls[0]['id'];
                } else {
                    $ins['girl_id'] = 0;
                }


                // 固有技能
                // 先取得固有技能的等級
                $skill_effect_jp = $this->qpDocument->find('div.skill_effect');
                if (!empty($skill_effect_jp)) {
                    $lv = pq("section.mt-3 .pb-4", $skill_effect_jp)->text();
                    if (!empty($lv)) {
                        $skill_up = explode(" ", $lv)[1];
                    } else {
                        $skill_up = "";
                    }
                } else {
                    $skill_up = "";
                }
                $i = 0;
                foreach (pq('div.shadow-lg', $skill_effect_jp) as $item) {
                    if ($i==0) {    // 固有技能
                        $ins_evt['s_skill_jp'][$i] = pq("span", $item)->text() ;
                        $ins_evt['s_skill_up'][$i] = $skill_up ;    // 固有技能的等級
                        $ins_evt['skill_caption_jp'][$i] = pq("p.text-sm", $item)->text() ;
                    } else {
                        $ins_evt['s_skill_jp'][$i] = pq("span.font-normal", $item)->text() ;
                        $ins_evt['s_skill_up'][$i] = pq("span.pl-4", $item)->text() ;
                        $ins_evt['skill_caption_jp'][$i] = pq("p.text-sm", $item)->text() ;
                    }
                    $i++;
                }

                // 效果
                $i = 0;
                foreach (pq('div.mr-2', $skill_effect_jp) as $item) {
                    $ins_evt['s_effect_jp'][$i] = pq("span", $item)->text() ;
                    $i++;
                }

                // 所有事件
                $all_event_jp = $this->qpDocument->find('div.mt-2') ;
                $i = 0;
                foreach (pq('section.mb-2', $all_event_jp) as $item) {
                    $ins_evt['s_event_jp'][$i] = pq("div.h-full .font-bold", $item)->text() ;
                    $j = 0;
                    foreach (pq('div.border-gray-500', $item) as $itemSub) {
                        $ins_evt['s_event_name_jp'][$i][] = pq("h5.allevent", $itemSub)->text();
                        $sub2JP = pq($itemSub)->find('div.allevent2');
                        $icon = "";
                        foreach (pq('span', $sub2JP) as $itemSub2) {
                            $sourceIconUrl = "";
                            $sourceIconUrl = pq($itemSub2)->find('imgscript img')->attr('src') ;

                            if(pq($itemSub2)->text() != "") {
                                $ins_evt['s_ability_icon_jp'][$i][$j][] = $icon;
                                $ins_evt['s_ability_jp'][$i][$j][] = pq($itemSub2)->text();
                                $icon = ""; // 當有技能寫入DB後，清除icon記號後，再重新取得icon
                            }

                            if($sourceIconUrl != "") {
                                $icon = explode('?', basename($sourceIconUrl))[0];
                            }
                         }
                        $j++;
                    }
                    $i++;
                }


                //
                // 繁中 - 每一張支援卡
                //
                $supportFile = $this->dataPathDocs."supports/".$ins['grab_id']."_tw.html" ;
                $readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=tw")) ; // 經過relaySite的網址
                $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                if (empty($content)) {
                    for ($retry = 0; $retry <= 2; $retry++) {
                        echo "      重試中......\n";
                        $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                        if (!empty($content)) {
                            break;
                        }
                    }
                }
                $content = str_replace( "noscript", "imgscript", $content) ;	        // 把noscript改成imgscript
                mac_write_file( $supportFile, $content) ;	        // 寫入檔案
                // 取得繁中的每一張支援卡的所有事件
                $content_tw = file_get_contents( $supportFile) ;	// 讀取檔案
                $content_tw = str_replace( "md:sr-only", "umaname", $content_tw) ;	// 把md:sr-only改成umaname
                $content_tw = str_replace( "md:w-4/12", "allevent", $content_tw) ;	// 把md:w-4/12改成allevent
                $content_tw = str_replace( "md:w-8/12 ", "allevent2", $content_tw) ;	// 把md:w-8/12改成all-event2
                $content_tw = str_replace( "md:w-9/12", "skill_effect", $content_tw) ;	// 把md:w-9/12改成skill_effect
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_tw) ;
                // 固有技能
                $skill_effect_tw = $this->qpDocument->find('div.skill_effect');
                $i = 0;
                foreach (pq('div.shadow-lg', $skill_effect_tw) as $item) {
                    if ($i==0) {    // 固有技能
                        $ins_evt['s_skill_tw'][$i] = pq("span", $item)->text() ;
                        $ins_evt['skill_caption_tw'][$i] = pq("p.text-sm", $item)->text() ;
                    } else {
                        $ins_evt['s_skill_tw'][$i] = pq("span.font-normal", $item)->text() ;
                        $ins_evt['skill_caption_tw'][$i] = pq("p.text-sm", $item)->text() ;
                    }
                    $i++;
                }
                // 效果
                $i = 0;
                foreach (pq('div.mr-2', $skill_effect_tw) as $item) {
                    $ins_evt['s_effect_tw'][$i] = pq("span", $item)->text() ;
                    $i++;
                }
                // 所有事件
                $supportNode_tw2 = $this->qpDocument->find('div.mt-2') ;
                $i = 0;
                foreach (pq('section.mb-2', $supportNode_tw2) as $item) {
                    $ins_evt['s_event_tw'][$i] = pq("div.h-full .font-bold", $item)->text() ;
                    $j = 0;
                    foreach (pq('div.border-gray-500', $item) as $itemSub) {
                        $ins_evt['s_event_name_tw'][$i][] = pq("h5.allevent", $itemSub)->text();
                        $sub2TW = pq($itemSub)->find('div.allevent2');
                        foreach (pq('span', $sub2TW) as $itemSub2) {
                            if(pq($itemSub2)->text() != "") {
                                $ins_evt['s_ability_tw'][$i][$j][] = pq($itemSub2)->text();
                            }
                        }
                        $j++;
                    }
                    $i++;
                }


                //
                // 簡中 - 每一張支援卡
                //
                $supportFile = $this->dataPathDocs."supports/".$ins['grab_id']."_cn.html" ;
                $readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=cn")) ; // 經過relaySite的網址
                $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                if (empty($content)) {
                    for ($retry = 0; $retry <= 2; $retry++) {
                        echo "      重試中......\n";
                        $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                        if (!empty($content)) {
                            break;
                        }
                    }
                }
                $content = str_replace( "noscript", "imgscript", $content) ;	        // 把noscript改成imgscript
                mac_write_file( $supportFile, $content) ;	        // 寫入檔案
                // 取得簡中的每一張支援卡的所有事件
                $content_cn = file_get_contents( $supportFile) ;	// 讀取檔案
                $content_cn = str_replace( "md:sr-only", "umaname", $content_cn) ;	// 把md:sr-only改成umaname
                $content_cn = str_replace( "md:w-4/12", "allevent", $content_cn) ;	// 把md:w-4/12改成allevent
                $content_cn = str_replace( "md:w-8/12 ", "allevent2", $content_cn) ;	// 把md:w-8/12改成all-event2
                $content_cn = str_replace( "md:w-9/12", "skill_effect", $content_cn) ;	// 把md:w-9/12改成skill_effect
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_cn) ;
                // 固有技能
                $skill_effect_cn = $this->qpDocument->find('div.skill_effect');
                $i = 0;
                foreach (pq('div.shadow-lg', $skill_effect_cn) as $item) {
                    if ($i==0) {    // 固有技能
                        $ins_evt['s_skill_cn'][$i] = pq("span", $item)->text() ;
                        $ins_evt['skill_caption_cn'][$i] = pq("p.text-sm", $item)->text() ;
                    } else {
                        $ins_evt['s_skill_cn'][$i] = pq("span.font-normal", $item)->text() ;
                        $ins_evt['skill_caption_cn'][$i] = pq("p.text-sm", $item)->text() ;
                    }
                    $i++;
                }
                // 效果
                $i = 0;
                foreach (pq('div.mr-2', $skill_effect_cn) as $item) {
                    $ins_evt['s_effect_cn'][$i] = pq("span", $item)->text() ;
                    $i++;
                }
                // 所有事件
                $supportNode_cn2 = $this->qpDocument->find('div.mt-2') ;
                $i = 0;
                foreach (pq('section.mb-2', $supportNode_cn2) as $item) {
                    $ins_evt['s_event_cn'][$i] = pq("div.h-full .font-bold", $item)->text() ;
                    $j = 0;
                    foreach (pq('div.border-gray-500', $item) as $itemSub) {
                        $ins_evt['s_event_name_cn'][$i][] = pq("h5.allevent", $itemSub)->text();
                        $sub2CN = pq($itemSub)->find('div.allevent2');
                        foreach (pq('span', $sub2CN) as $itemSub2) {
                            if(pq($itemSub2)->text() != "") {
                                $ins_evt['s_ability_cn'][$i][$j][] = pq($itemSub2)->text();
                            }
                        }
                        $j++;
                    }
                    $i++;
                }


                //
                // 韓文 - 每一張支援卡
                //
                $supportFile = $this->dataPathDocs."supports/".$ins['grab_id']."_kr.html" ;
                $readUrl = sprintf( $this->relaySite, $ins['grab_url'].urlencode("?server=ko")) ; // 經過relaySite的網址
                $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                if (empty($content)) {
                    for ($retry = 0; $retry <= 2; $retry++) {
                        echo "      重試中......\n";
                        $content = $this->getPage( $readUrl, '', '', $this->pageGirlsIndexUrl) ;
                        if (!empty($content)) {
                            break;
                        }
                    }
                }
                $content = str_replace( "noscript", "imgscript", $content) ;	        // 把noscript改成imgscript
                mac_write_file( $supportFile, $content) ;	        // 寫入檔案
                // 取得韓文的每一張支援卡的所有事件
                $content_kr = file_get_contents( $supportFile) ;	// 讀取檔案
                $content_kr = str_replace( "md:sr-only", "umaname", $content_kr) ;	// 把md:sr-only改成umaname
                $content_kr = str_replace( "md:w-4/12", "allevent", $content_kr) ;	// 把md:w-4/12改成allevent
                $content_kr = str_replace( "md:w-8/12 ", "allevent2", $content_kr) ;	// 把md:w-8/12改成all-event2
                $content_kr = str_replace( "md:w-9/12", "skill_effect", $content_kr) ;	// 把md:w-9/12改成skill_effect
                $this->qpDocument = \phpQuery::newDocumentHTML( $content_kr) ;
                // 固有技能
                $skill_effect_kr = $this->qpDocument->find('div.skill_effect');
                $i = 0;
                foreach (pq('div.shadow-lg', $skill_effect_kr) as $item) {
                    if ($i==0) {    // 固有技能
                        $ins_evt['s_skill_kr'][$i] = pq("span", $item)->text() ;
                        $ins_evt['skill_caption_kr'][$i] = pq("p.text-sm", $item)->text() ;
                    } else {
                        $ins_evt['s_skill_kr'][$i] = pq("span.font-normal", $item)->text() ;
                        $ins_evt['skill_caption_kr'][$i] = pq("p.text-sm", $item)->text() ;
                    }
                    $i++;
                }
                // 效果
                $i = 0;
                foreach (pq('div.mr-2', $skill_effect_kr) as $item) {
                    $ins_evt['s_effect_kr'][$i] = pq("span", $item)->text() ;
                    $i++;
                }
                // 所有事件
                $supportNode_kr2 = $this->qpDocument->find('div.mt-2') ;
                $i = 0;
                foreach (pq('section.mb-2', $supportNode_kr2) as $item) {
                    $ins_evt['s_event_kr'][$i] = pq("div.h-full .font-bold", $item)->text() ;
                    $j = 0;
                    foreach (pq('div.border-gray-500', $item) as $itemSub) {
                        $ins_evt['s_event_name_kr'][$i][] = pq("h5.allevent", $itemSub)->text();
                        $sub2KR = pq($itemSub)->find('div.allevent2');
                        foreach (pq('span', $sub2KR) as $itemSub2) {
                            if(pq($itemSub2)->text() != "") {
                                $ins_evt['s_ability_kr'][$i][$j][] = pq($itemSub2)->text();
                            }
                        }
                        $j++;
                    }
                    $i++;
                }

                $msg = "{$ins['grab_id']}.html 下載完成" ;
                echo $msg.PHP_EOL ;
                Log::channel('grabbers')->write( "{$ins['grab_id']}.html 下載完成", 'info') ;
            }

            /*|--------------------------------------------------------------------------
            |*| 檢查是否已經存在
            |*|--------------------------------------------------------------------------*/
            if ( !array_key_exists( $ins['grab_id'], $objSupList)) {
                try {
                    $support_id = Db::name('uma_support')->insert($ins, true) ;
                    $msg = "[Umaevents] getGirlsSupport TW: {$ins['s_user_tw']} {$ins['grab_id']} new girl support[".($newSupCnt+1)."/".$totalSupCnt."]".PHP_EOL ;
                    echo $msg ;
                    Log::channel('grabbers')->write( $msg, 'info') ;
                    $newSupCnt++ ;

                    // 寫入每一支援卡的所有技能(含固有技能)
                    if(!empty($ins_evt['s_skill_jp'])) {
                        for ($i = 0; $i < count($ins_evt['s_skill_jp']); $i++) {
                            $evt_skill = [];
                            $evt_skill['support_id'] = $support_id;
                            $evt_skill['girl_id'] = $ins['girl_id'];
                            $evt_skill['skill_up'] = !empty($ins_evt['s_skill_up'][$i]) ? $ins_evt['s_skill_up'][$i] : "";
                            $evt_skill['skill_tw'] = !empty($ins_evt['s_skill_tw'][$i]) ? $ins_evt['s_skill_tw'][$i] : "";
                            $evt_skill['skill_caption_tw'] = !empty($ins_evt['s_skill_caption_tw'][$i]) ? $ins_evt['s_skill_caption_tw'][$i] : "";
                            $evt_skill['skill_cn'] = !empty($ins_evt['s_skill_cn'][$i]) ? $ins_evt['s_skill_cn'][$i] : "";
                            $evt_skill['skill_caption_cn'] = !empty($ins_evt['s_skill_caption_cn'][$i]) ? $ins_evt['s_skill_caption_cn'][$i] : "";
                            $evt_skill['skill_jp'] = !empty($ins_evt['s_skill_jp'][$i]) ? $ins_evt['s_skill_jp'][$i] : "";
                            $evt_skill['skill_caption_jp'] = !empty($ins_evt['s_skill_caption_jp'][$i]) ? $ins_evt['s_skill_caption_jp'][$i] : "";
                            $evt_skill['skill_kr'] = !empty($ins_evt['s_skill_kr'][$i]) ? $ins_evt['s_skill_kr'][$i] : "";
                            $evt_skill['skill_caption_kr'] = !empty($ins_evt['s_skill_caption_kr'][$i]) ? $ins_evt['s_skill_caption_kr'][$i] : "";
                            Db::name('uma_support_skill')->insert($evt_skill);
                        }
                    }

                    // 寫入每一支援卡的所有效果
                    if(!empty($ins_evt['s_effect_jp'])) {
                        for ($i = 0; $i < count($ins_evt['s_effect_jp']); $i++) {
                            $evt_effect = [];
                            $evt_effect['support_id'] = $support_id;
                            $evt_effect['girl_id'] = $ins['girl_id'];
                            $evt_effect['effect_tw'] = !empty($ins_evt['s_effect_tw'][$i]) ? $ins_evt['s_effect_tw'][$i] : "";
                            $evt_effect['effect_cn'] = !empty($ins_evt['s_effect_cn'][$i]) ? $ins_evt['s_effect_cn'][$i] : "";
                            $evt_effect['effect_jp'] = !empty($ins_evt['s_effect_jp'][$i]) ? $ins_evt['s_effect_jp'][$i] : "";
                            $evt_effect['effect_kr'] = !empty($ins_evt['s_effect_kr'][$i]) ? $ins_evt['s_effect_kr'][$i] : "";
                            Db::name('uma_support_effect')->insert($evt_effect);
                        }
                    }

                    // 寫入支援卡的所有事件
                    if(!empty($ins_evt['s_event_jp'])) {
                        for ($i = 0; $i < count($ins_evt['s_event_jp']); $i++) {
                            $evt = [];
                            $evt['support_id'] = $support_id;
                            $evt['girl_id'] = $ins['girl_id'];
                            $evt['event_title_tw'] = $ins_evt['s_event_tw'][$i];       // ???
                            $evt['event_title_cn'] = $ins_evt['s_event_cn'][$i];       // ???
                            $evt['event_title_jp'] = $ins_evt['s_event_jp'][$i];       // ???
                            $evt['event_title_kr'] = $ins_evt['s_event_kr'][$i];       // ???
                            // 寫入每一支援卡的所有事件
                            for ($j = 0; $j < count($ins_evt['s_event_name_jp'][$i]); $j++) {
                                $evt['event_name_tw'] = !empty($ins_evt['s_event_name_tw'][$i][$j]) ? $ins_evt['s_event_name_tw'][$i][$j] : "";
                                $evt['event_name_cn'] = !empty($ins_evt['s_event_name_cn'][$i][$j]) ? $ins_evt['s_event_name_cn'][$i][$j] : "";
                                $evt['event_name_jp'] = !empty($ins_evt['s_event_name_jp'][$i][$j]) ? $ins_evt['s_event_name_jp'][$i][$j] : "";
                                $evt['event_name_kr'] = !empty($ins_evt['s_event_name_kr'][$i][$j]) ? $ins_evt['s_event_name_kr'][$i][$j] : "";
                                $support_event_id = Db::name('uma_support_event')->insert($evt, true);
                                // 寫入每一支援卡的所有技能
                                for ($k = 0; $k < count($ins_evt['s_ability_jp'][$i][$j]); $k++) {
                                    $evt_ability = [];
                                    $evt_ability['support_event_id'] = $support_event_id;
                                    $evt_ability['support_id'] = $support_id;
                                    $evt_ability['girl_id'] = $ins['girl_id'];
                                    $evt_ability['ability_tw'] = !empty($ins_evt['s_ability_tw'][$i][$j][$k]) ? $ins_evt['s_ability_tw'][$i][$j][$k] : "";
                                    $evt_ability['ability_cn'] = !empty($ins_evt['s_ability_cn'][$i][$j][$k]) ? $ins_evt['s_ability_cn'][$i][$j][$k] : "";
                                    $evt_ability['ability_jp'] = !empty($ins_evt['s_ability_jp'][$i][$j][$k]) ? $ins_evt['s_ability_jp'][$i][$j][$k] : "";
                                    $evt_ability['ability_kr'] = !empty($ins_evt['s_ability_kr'][$i][$j][$k]) ? $ins_evt['s_ability_kr'][$i][$j][$k] : "";
                                    $evt_ability['icon'] = $ins_evt['s_ability_icon_jp'][$i][$j][$k];
                                    Db::name('uma_support_event_ability')->insert($evt_ability);
                                }
                            }
                        }
                    }
                } catch ( \Throwable $th) {
                    $SQLCmd = Db::getLastSql();
                    Log::channel('grabbers')->write( $SQLCmd, 'mysql') ;
                    exit() ;
                }
            } else {
                echo  "[Umaevents] getGirlsSupport TW: Can't found!!!!!!".PHP_EOL ;
            }

//			if ( $newSupCnt > 0 ) exit() ;
		}

        /*|--------------------------------------------------------------------------
        |*| 結算筆數
        |*|--------------------------------------------------------------------------*/
        if ( $newSupCnt > 0) {
            $msg = "[Umaevents] getGirlsSupport TW: ".$newSupCnt." new girls support".PHP_EOL ;
            echo $msg ;
            Log::channel('grabbers')->write( $msg, 'info') ;
        } else {
            $msg = "[Umaevents] getGirlsSupport TW: no new girls support".PHP_EOL ;
            echo $msg ;
            Log::channel('grabbers')->write( $msg, 'info') ;
        }
	}




	public function buildGirlInfo( $girlNode = [])
	{
		$girlRow = [] ;
		$girlUrl = $girlNode['grab_url'] ;
		$girlPrefix = basename( parse_url( $girlUrl, PHP_URL_PATH)) ;
		$savePath = $this->dataPathDocs ;
		$saveFile = $savePath."roles/".$girlPrefix."_tw.html" ;

        $summary_jp = "";
		if ( !file_exists( $saveFile) || filesize( $saveFile) == 0) {
			$readUrl = sprintf( $this->relaySite, $girlUrl) ; // 經過relaySite的網址
			/*|--------------------------------------------------------------------------
			|*| $url = '', $cookie = '', $post = '', $referer = '', $useragent = null, $saveFile = ''
			|*|--------------------------------------------------------------------------*/
            // 繁中
			$referer = $this->pageGirlsIndexUrl ;
			$content = $this->getPage( $readUrl, '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $saveFile, $content) ;	// 寫入檔案
            // 簡中
            $saveFile = $savePath."roles/".$girlPrefix."_cn.html" ;
            $content = $this->getPage( $readUrl.urlencode("?server=cn"), '', '', $referer) ;
            $content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
            mac_write_file( $saveFile, $content) ;	// 寫入檔案
			// 日文
			$saveFile = $savePath."roles/".$girlPrefix."_jp.html" ;
			$content = $this->getPage( $readUrl.urlencode("?server=jp"), '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $saveFile, $content) ;	// 寫入檔案
            // 取得日文的summary
            $content = file_get_contents( $savePath."roles/".$girlPrefix."_jp.html") ;	// 讀取檔案
            $this->qpDocument = \phpQuery::newDocumentHTML( $content) ;
            $mainNode_jp = $this->getNodeElement('.min-h-screen div div section') ;
            $summary_jp = $this->getPageElement('p', $mainNode_jp) ;

			// 韓文
			$saveFile = $savePath."roles/".$girlPrefix."_kr.html" ;
			$content = $this->getPage( $readUrl.urlencode("?server=ko"), '', '', $referer) ;
			$content = str_replace( "noscript", "imgscript", $content) ;	// 把noscript改成imgscript
			mac_write_file( $saveFile, $content) ;	// 寫入檔案

			Log::channel('grabbers')->write( "{$girlPrefix}.html 下載完成", 'info') ;
		}

		$content = file_get_contents( $savePath."roles/".$girlPrefix."_jp.html") ;	// 讀取檔案
        $content = str_replace( "md:w-9/12", "venue_distance", $content) ;	// 把md:w-9/12改成venue_distance
        $content = str_replace( "md:w-8/12 ", "allevent2", $content) ;	    // 把md:w-8/12改成all-event2
		$this->qpDocument = \phpQuery::newDocumentHTML( $content) ;
		$mainNode = $this->getNodeElement('.min-h-screen div div section') ;

		$upd = [] ;
		$upd['id']= $girlNode['id'] ;
		$upd['name'] = $this->getPageElement('h1 div', $mainNode) ;
		$upd['name_jp'] = $this->getPageElement('h3', $mainNode) ;
		$upd['summary'] = $this->getPageElement('p', $mainNode) ;
        $upd['summary_jp'] = $summary_jp ;
        // 取得角色設定區域
		$infoNode = $this->getNodeElement( 'div div.mt-3.pb-3 div.grid.grid-cols-2', $mainNode) ;
		$items = pq('div.flex', $infoNode) ;
		// 應取回的欄位資料
		$infoMap = ['voice' => 'CV', 'birthday' => '生日', 'height' => '身高', 'bwh' => '三圍',] ;
		foreach ($items as $nIdx => $item) {
			$colName = pq('div', $item)->eq(0)->text() ;
			foreach ($infoMap as $key => $value) {
				if ( $value == $colName) {
					$upd[ $key] = pq('div', $item)->eq(1)->text() ;
					// 處理三圍 [B97 / W61 / H91]
					if ( $key == 'bwh') {
						preg_match_all( '/\d+/', $upd[ $key], $matches);
						$upd['b'] = $matches[0][0] ;
						$upd['w'] = $matches[0][1] ;
						$upd['h'] = $matches[0][2] ;
						unset( $upd[ $key]) ;
					} else if ( $key == 'height') {
						$upd[ $key] = str_replace( 'cm', '', $upd[ $key]) ;
					} else if ( $key == 'birthday') {
						// 5月27日 -> 1900-05-27
						$upd[ $key] = str_replace('月', '-', $upd[ $key]) ;
						$upd[ $key] = str_replace('日', '', $upd[ $key]) ;
						$upd[ $key] = '1900-'.$upd[$key] ;
					}
				}
			}
		}

        // 取得[基礎能力]區域
        $infoNode = $this->getNodeElement( 'div section.max-w-2xl div.pt-4', $mainNode) ;
        $i = 0;
        foreach ($infoNode as $item) {
            $itemNode = $this->getNodeElement( 'div.grid-cols-5 .value-box', $item) ;
            $star = pq("span.text-base", $itemNode);
            $level = "";
            foreach ($star as $aaa) {
                if ($level != "")
                    $level .= ",";
                $level .= pq($aaa)->text();
            }

            if($i == 0)     $upd['level_1_star'] = $level;
            else            $upd['level_5_star'] = $level;
            $i++;
        }

        // 取得[場地,距離,腳質]區域
        $infoNode = $this->getNodeElement( 'div.venue_distance section.grid-cols-5', $mainNode) ;
        $i = 0;
        foreach ($infoNode as $item) {
            $itemNode = $this->getNodeElement( 'div.inline-block', $item) ;
            $j = 0;
            foreach ($itemNode as $aaa) {
                if($i == 0) {
                    if ($j == 0)            $upd['lawn'] = pq($aaa)->text();
                    else if ($j == 1)       $upd['muddy_ground'] = pq($aaa)->text();
                } else if($i == 1) {
                    if ($j == 0)            $upd['distance_short'] = pq($aaa)->text();
                    else if ($j == 1)       $upd['distance_miles'] = pq($aaa)->text();
                    else if ($j == 2)       $upd['distance_middle'] = pq($aaa)->text();
                    else if ($j == 3)       $upd['distance_long'] = pq($aaa)->text();
                } else if($i == 2) {
                    if ($j == 0)            $upd['foot_head'] = pq($aaa)->text();
                    else if ($j == 1)       $upd['foot_front'] = pq($aaa)->text();
                    else if ($j == 2)       $upd['foot_center'] = pq($aaa)->text();
                    else if ($j == 3)       $upd['foot_after'] = pq($aaa)->text();
                }
                $j++;
            }
            $i++;
        }

        // 取得[基礎能力：加成]區域
        $infoNode = $this->getNodeElement( 'div.grid-cols-5 div.py-2', $mainNode) ;
        $level = "";
        foreach ($infoNode as $item) {
            $itemNode = $this->getNodeElement( 'span.font-bold', $item) ;
            if ($level != "")
                $level .= ",";
            $level .= pq($itemNode)->text();
        }
        $upd['growth_rate'] = $level;

        //
        // 取得[所有事件]區域
        $all_event_jp = $this->qpDocument->find('div.mt-2') ;
        $i = 0;
        foreach (pq('section.mb-2', $all_event_jp) as $item) {
            $ins_evt['s_event_jp'][$i] = pq("div.h-full h4.text-white", $item)->text() ;
            $j = 0;
            foreach (pq('div.border-gray-500', $item) as $itemSub) {
                $ins_evt['s_event_name_jp'][$i][] = pq("h5.py-2", $itemSub)->text();
                $sub2JP = pq($itemSub)->find('div.allevent2');
                $icon = "";
                foreach (pq('span', $sub2JP) as $itemSub2) {
                    $sourceIconUrl = pq($itemSub2)->find('imgscript img')->attr('src') ;
                    if(pq($itemSub2)->text() != "") {
                        $ins_evt['s_ability_icon_jp'][$i][$j][] = $icon;
                        $ins_evt['s_ability_jp'][$i][$j][] = pq($itemSub2)->text();
                        $icon = ""; // 當有技能寫入DB後，清除icon記號後，再重新取得icon
                    }
                    if($sourceIconUrl != "") {
                        $icon = explode('?', basename($sourceIconUrl))[0];
                    }
                }
                $j++;
            }
            $i++;
        }
        // 繁中
        $content = file_get_contents( $savePath."roles/".$girlPrefix."_tw.html") ;	// 讀取檔案
        $content = str_replace( "md:w-9/12", "venue_distance", $content) ;	// 把md:w-9/12改成venue_distance
        $content = str_replace( "md:w-8/12 ", "allevent2", $content) ;	    // 把md:w-8/12改成all-event2
        $this->qpDocument = \phpQuery::newDocumentHTML( $content) ;
        $all_event_tw = $this->qpDocument->find('div.mt-2') ;
        $i = 0;
        foreach (pq('section.mb-2', $all_event_tw) as $item) {
            $ins_evt['s_event_tw'][$i] = pq("div.h-full h4.text-white", $item)->text() ;
            $j = 0;
            foreach (pq('div.border-gray-500', $item) as $itemSub) {
                $ins_evt['s_event_name_tw'][$i][] = pq("h5.py-2", $itemSub)->text();
                $sub2TW = pq($itemSub)->find('div.allevent2');
                $icon = "";
                foreach (pq('span', $sub2TW) as $itemSub2) {
                    $sourceIconUrl = pq($itemSub2)->find('imgscript img')->attr('src') ;
                    if(pq($itemSub2)->text() != "") {
                        $ins_evt['s_ability_icon_tw'][$i][$j][] = $icon;
                        $ins_evt['s_ability_tw'][$i][$j][] = pq($itemSub2)->text();
                        $icon = ""; // 當有技能寫入DB後，清除icon記號後，再重新取得icon
                    }
                    if($sourceIconUrl != "") {
                        $icon = explode('?', basename($sourceIconUrl))[0];
                    }
                }
                $j++;
            }
            $i++;
        }
        // 簡中
        $content = file_get_contents( $savePath."roles/".$girlPrefix."_cn.html") ;	// 讀取檔案
        $content = str_replace( "md:w-9/12", "venue_distance", $content) ;	// 把md:w-9/12改成venue_distance
        $content = str_replace( "md:w-8/12 ", "allevent2", $content) ;	    // 把md:w-8/12改成all-event2
        $this->qpDocument = \phpQuery::newDocumentHTML( $content) ;
        $all_event_cn = $this->qpDocument->find('div.mt-2') ;
        $i = 0;
        foreach (pq('section.mb-2', $all_event_cn) as $item) {
            $ins_evt['s_event_cn'][$i] = pq("div.h-full h4.text-white", $item)->text() ;
            $j = 0;
            foreach (pq('div.border-gray-500', $item) as $itemSub) {
                $ins_evt['s_event_name_cn'][$i][] = pq("h5.py-2", $itemSub)->text();
                $sub2CN = pq($itemSub)->find('div.allevent2');
                $icon = "";
                foreach (pq('span', $sub2CN) as $itemSub2) {
                    $sourceIconUrl = pq($itemSub2)->find('imgscript img')->attr('src') ;
                    if(pq($itemSub2)->text() != "") {
                        $ins_evt['s_ability_icon_cn'][$i][$j][] = $icon;
                        $ins_evt['s_ability_cn'][$i][$j][] = pq($itemSub2)->text();
                        $icon = ""; // 當有技能寫入DB後，清除icon記號後，再重新取得icon
                    }
                    if($sourceIconUrl != "") {
                        $icon = explode('?', basename($sourceIconUrl))[0];
                    }
                }
                $j++;
            }
            $i++;
        }
        // 韓文
        $content = file_get_contents( $savePath."roles/".$girlPrefix."_kr.html") ;	// 讀取檔案
        $content = str_replace( "md:w-9/12", "venue_distance", $content) ;	// 把md:w-9/12改成venue_distance
        $content = str_replace( "md:w-8/12 ", "allevent2", $content) ;	    // 把md:w-8/12改成all-event2
        $this->qpDocument = \phpQuery::newDocumentHTML( $content) ;
        $all_event_kr = $this->qpDocument->find('div.mt-2') ;
        $i = 0;
        foreach (pq('section.mb-2', $all_event_kr) as $item) {
            $ins_evt['s_event_kr'][$i] = pq("div.h-full h4.text-white", $item)->text() ;
            $j = 0;
            foreach (pq('div.border-gray-500', $item) as $itemSub) {
                $ins_evt['s_event_name_kr'][$i][] = pq("h5.py-2", $itemSub)->text();
                $sub2KR = pq($itemSub)->find('div.allevent2');
                $icon = "";
                foreach (pq('span', $sub2KR) as $itemSub2) {
                    $sourceIconUrl = pq($itemSub2)->find('imgscript img')->attr('src') ;
                    if(pq($itemSub2)->text() != "") {
                        $ins_evt['s_ability_icon_kr'][$i][$j][] = $icon;
                        $ins_evt['s_ability_kr'][$i][$j][] = pq($itemSub2)->text();
                        $icon = ""; // 當有技能寫入DB後，清除icon記號後，再重新取得icon
                    }
                    if($sourceIconUrl != "") {
                        $icon = explode('?', basename($sourceIconUrl))[0];
                    }
                }
                $j++;
            }
            $i++;
        }

        // var_dump( $upd) ;
		try {
			Db::name('uma_girls')->update( $upd) ;
            // 寫入馬娘的所有事件
            if(!empty($ins_evt['s_event_jp'])) {
                for ($i = 0; $i < count($ins_evt['s_event_jp']); $i++) {
                    $evt = [];
                    $evt['girl_id'] = $upd['id'];
                    $evt['event_title_tw'] = $ins_evt['s_event_tw'][$i];       // ???
                    $evt['event_title_cn'] = $ins_evt['s_event_cn'][$i];       // ???
                    $evt['event_title_jp'] = $ins_evt['s_event_jp'][$i];       // ???
                    $evt['event_title_kr'] = $ins_evt['s_event_kr'][$i];       // ???
                    // 寫入每一支援卡的所有事件
                    for ($j = 0; $j < count($ins_evt['s_event_name_jp'][$i]); $j++) {
                        $evt['event_name_tw'] = !empty($ins_evt['s_event_name_tw'][$i][$j]) ? $ins_evt['s_event_name_tw'][$i][$j] : "";
                        $evt['event_name_cn'] = !empty($ins_evt['s_event_name_cn'][$i][$j]) ? $ins_evt['s_event_name_cn'][$i][$j] : "";
                        $evt['event_name_jp'] = !empty($ins_evt['s_event_name_jp'][$i][$j]) ? $ins_evt['s_event_name_jp'][$i][$j] : "";
                        $evt['event_name_kr'] = !empty($ins_evt['s_event_name_kr'][$i][$j]) ? $ins_evt['s_event_name_kr'][$i][$j] : "";
                        $girl_event_id = Db::name('uma_girls_event')->insert($evt, true);

                        // 寫入每一支援卡的所有技能
                        if(!empty($ins_evt['s_ability_jp'][$i][$j])) {
                            for ($k = 0; $k < count($ins_evt['s_ability_jp'][$i][$j]); $k++) {
                                $evt_ability = [];
                                $evt_ability['girl_event_id'] = $girl_event_id;
                                $evt_ability['girl_id'] = $upd['id'];
                                $evt_ability['ability_tw'] = !empty($ins_evt['s_ability_tw'][$i][$j][$k]) ? $ins_evt['s_ability_tw'][$i][$j][$k] : "";
                                $evt_ability['ability_cn'] = !empty($ins_evt['s_ability_cn'][$i][$j][$k]) ? $ins_evt['s_ability_cn'][$i][$j][$k] : "";
                                $evt_ability['ability_jp'] = !empty($ins_evt['s_ability_jp'][$i][$j][$k]) ? $ins_evt['s_ability_jp'][$i][$j][$k] : "";
                                $evt_ability['ability_kr'] = !empty($ins_evt['s_ability_kr'][$i][$j][$k]) ? $ins_evt['s_ability_kr'][$i][$j][$k] : "";
                                $evt_ability['icon'] = $ins_evt['s_ability_icon_jp'][$i][$j][$k];
                                Db::name('uma_girls_event_ability')->insert($evt_ability);
                            }
                        }
                    }
                }
            }
		} catch ( \Throwable $th) {
            echo "============================ !!!!!!!!!!!!!!!\n";
			$SQLCmd = Db::getLastSql();
			Log::channel('grabbers')->write( $SQLCmd, 'mysql') ;
			exit() ;
		}
	}

}
